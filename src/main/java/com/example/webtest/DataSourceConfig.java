package com.example.webtest;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.cloud.config.java.AbstractCloudConfig;
import org.springframework.context.annotation.Profile;

import javax.sql.DataSource;

@Configuration
//@Profile({"mysql-cloud", "postgres-cloud", "oracle-cloud", "sqlserver-cloud"})
//@Profile({"postgres-cloud"})
public class DataSourceConfig
{

    @Bean
    @Profile("local")
    public DataSource getDataSourceDev() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName("org.postgresql.Driver");
        dataSourceBuilder.url("jdbc:postgresql://localhost:5432/postgres");
        dataSourceBuilder.username("postgres");
        dataSourceBuilder.password("sys");
        return dataSourceBuilder.build();
    }

    @Bean
    @Profile("default")
    public DataSource getDataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName("org.postgresql.Driver");
        dataSourceBuilder.url("jdbc:postgresql://postgres:5432/nice_marmot");

        dataSourceBuilder.username("runner");
        dataSourceBuilder.password("sys");
        return dataSourceBuilder.build();
    }

}