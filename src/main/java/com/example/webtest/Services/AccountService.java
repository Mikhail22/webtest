package com.example.webtest.Services;

import com.example.webtest.DAO.AccountRepository;
import com.example.webtest.Models.Account;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.Set;

@Service
public class AccountService {
    
    AccountRepository accountRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository) {
        this.accountRepository=accountRepository;

    }

    public void save(Set<Account> accounts){
        accountRepository.saveAll(accounts);
    }

    public void save(Account account){
        accountRepository.save(account);
    }

    public Set<Account> findAll(){
        Set<Account> accounts = new HashSet<>();
        accountRepository.findAll().forEach(account -> accounts.add(account));
        return accounts;
    }


    @Value("${spring.profiles.active}")
    private String activeProfile;
    @Value("${welcome.message}")
    private String welcome_message;

    @PostConstruct
    void init(){
        System.out.println("init "+activeProfile+"   "+welcome_message);
        Account account = Account.builder().name("adidas").email("adidas@com").build();
        save(account);
        account = Account.builder().name("nike").email("nike@com").build();
        save(account);
        account = Account.builder().name("puma").email("puma@com").build();
        save(account);
    }
}
