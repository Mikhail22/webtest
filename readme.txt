winpty cf login -a https://api.run.pivotal.io

cf push web-test -p ./target/webtest-0.0.1-SNAPSHOT.jar -b https://github.com/cloudfoundry/java-buildpack.git

https://github.com/cloudfoundry/java-buildpack#additional-documentation

cf push <APP-NAME> -p <ARTIFACT> -b https://github.com/cloudfoundry/java-buildpack.git


Connect a Database
PCF enables administrators to provide a variety of services on the platform that can easily be consumed by applications.

List the available ElephantSQL plans:

cf marketplace -s elephantsql
Create a service instance with the free plan:

cf create-service elephantsql turtle cf-demo-db
Bind the newly created service to the app:

cf bind-service cf-demo cf-demo-db
Once a service is bound to an app, environment variables are stored that allow the app to connect to the service after a push, restage, or restart command.

Restage the app:

cf restage cf-demo
Verify the new service is bound to the app:

cf services